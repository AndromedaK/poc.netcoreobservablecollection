﻿using System;
using System.Collections.Generic;
using System.Text;

namespace poc.netcoreobservablecollection.Models
{
    public class BossDepartment
    {

        public string Name  { get; set; }


        public BossDepartment(string name)
        {
            this.Name = name;
        }

        public static List<BossDepartment> GenerateList()
        {
            var Lista = new List<BossDepartment>()
                {  new BossDepartment( "Informatica"),
                   new BossDepartment("Quimica"), 
                   new BossDepartment( "Robotica" ),};
  
                       return Lista;

        }
    }


}

﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace poc.netcoreobservablecollection.Models
{
    public class Department
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public ObjectId  Id { get; set; }

        [BsonElement("NombreDepartamento")]
        public string NombreDepartamento { get; set; }

        [BsonElement("DepartamentoJefe")]
        public string DepartamentoJefe { get; set; }

        public Department()
        {

        }

        public Department(ObjectId Id, string NombreDepartamento)
        {
            this.Id                 = Id;
            this.NombreDepartamento = NombreDepartamento;

        }

        public Department(ObjectId Id, string NombreDepartamento, string DepartamentoJefe)
        {
            this.Id                 = Id;
            this.NombreDepartamento = NombreDepartamento;
            this.DepartamentoJefe   = DepartamentoJefe;

        }

        public Department(ObservableCollection<Department> d)
        {
           
        }
    }
}

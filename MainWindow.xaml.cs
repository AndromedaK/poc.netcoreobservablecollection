﻿using MahApps.Metro.Controls;
using MongoDB.Bson;
using poc.netcoreobservablecollection.Models;
using poc.netcoreobservablecollection.Respositorio;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace poc.netcoreobservablecollection
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        private IDepartmentCollection db = new DepartmentCollection();

        //private ObservableCollection<Department> departments;

        public MainWindow()
        {
            InitializeComponent();
            FillComboBox();
            FillDataGrid();
            
        }


        #region Controls Methods
        private async void FillComboBox()
        {
            var items = await db.GetAllDepartments();
            cmboDepartamento.ItemsSource = items;
            cmboDepartamento.DisplayMemberPath = "NombreDepartamento";
        }
       
        private async void  FillDataGrid()
        {

            var items = await db.GetAllDepartments();          
            dgridDepartamentos.ItemsSource = items;          
        }

        private async void dgridDepartamentos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                await Task.Delay(TimeSpan.FromSeconds(0.5));

               // cmboDepartamento.Text = Nulos.DepartamentoJefe;

               Department Nulos=  (Department)dgridDepartamentos.SelectedItem;
               
                
                if (Nulos != null)
                {
                    txtID.Text = Nulos.Id.ToString();
                    txtNombreDepa.Text = Nulos.NombreDepartamento;
                    //cmboDepartamento.Text = Nulos.DepartamentoJefe;
                   
                }

            }
            catch (System.Exception)
            {

                throw;
            }

        }

        #endregion



        #region CRUD DEPARTAMENTOS
        private async void Boton_Crear(object sender, RoutedEventArgs e)
        {
            Department department = new Department();
            department.NombreDepartamento = txtNombreDepa.Text;
            if (cmboDepartamento.Text != null)
            {
                department.DepartamentoJefe = cmboDepartamento.Text;
            }

           // departments.Add(department);
            await db.InsertDepartment(department);
            FillComboBox();
            FillDataGrid();

        }

        private async void Boton_ObtenerRegistros(object sender, RoutedEventArgs e)
        {

            await db.GetAllDepartments();
            FillComboBox();
            FillDataGrid();
        }
        private async void Boton_Eliminar(object sender, RoutedEventArgs e)
        {
        
            await db.DeleteDepartment(txtID.Text);
            FillComboBox();
            FillDataGrid();
            // var d = departments.Where(x => x.Id == ObjectId.Parse(txtID.Text)).First();
            // departments.Remove(d);

        }

        private async void Boton_Actualizar(object sender, RoutedEventArgs e)
        {
            Department department = new Department();

            department.Id = ObjectId.Parse(txtID.Text);
            department.NombreDepartamento = txtNombreDepa.Text;
            if (cmboDepartamento.Text != null)
            {
                department.DepartamentoJefe = cmboDepartamento.Text;
            }
          
            await db.UpdateDepartment(department);
            FillComboBox();
            FillDataGrid();

        }



        #endregion


    }
}

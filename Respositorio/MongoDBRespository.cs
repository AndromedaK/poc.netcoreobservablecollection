﻿using MongoDB.Bson.IO;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;

namespace poc.netcoreobservablecollection.Respositorio
{
    public class MongoDBRespository
    {

        public readonly IMongoDatabase db;

        public readonly MongoClient client;
     
        public MongoDBRespository(string database)
        {
            client = new MongoClient("mongodb+srv://cristopher:cristopher2020@troya.gnddo.gcp.mongodb.net/test?authSource=admin&replicaSet=atlas-11vmwj-shard-0&readPreference=primary&appname=MongoDB%20Compass%20Community&ssl=true");
            db     = client.GetDatabase(database);
        }
    }

}

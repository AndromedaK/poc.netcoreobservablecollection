﻿using MongoDB.Bson;
using MongoDB.Driver;
using poc.netcoreobservablecollection.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;

namespace poc.netcoreobservablecollection.Respositorio
{
    public class DepartmentCollection : IDepartmentCollection
    {
        internal MongoDBRespository MongoDBRespository = new MongoDBRespository("ControlTareas");

        private IMongoCollection<Department> Collection;

        public DepartmentCollection()
        {
            Collection = MongoDBRespository.db.GetCollection<Department>("departamentos");
             
        }
        public async Task DeleteDepartment(string id)
        {
            var filter = Builders<Department>.Filter.Eq(s => s.Id, new ObjectId(id));
            await Collection.DeleteOneAsync(filter);

        }

        public async Task<List<Department>> GetAllDepartments()
        {
            return await Collection.FindAsync(new BsonDocument()).Result.ToListAsync();
        }

        public async Task<ObservableCollection<Department>> GetAllDepartmentsAutomatic()
        {
            var response =  await Collection.AsQueryable().ToListAsync<Department>();
            ObservableCollection<Department> departments = new ObservableCollection<Department>(response);
            return departments;
        }

        public async Task<Department> GetDepartmentById(string id)
        {
            return await Collection.FindAsync(new BsonDocument { { "_id", new ObjectId(id) } }).Result.FirstAsync();
        }

        public async Task InsertDepartment(Department department)
        {
            await Collection.InsertOneAsync(department);
        }

        public async Task UpdateDepartment(Department department)
        {
            var filter = Builders<Department>.Filter.Eq(s => s.Id, department.Id);
            await Collection.ReplaceOneAsync(filter, department);
        }
    }
}

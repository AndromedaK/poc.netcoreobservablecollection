﻿using poc.netcoreobservablecollection.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;

namespace poc.netcoreobservablecollection.Respositorio
{
    public interface IDepartmentCollection
    {

        Task InsertDepartment(Department department);
        Task UpdateDepartment(Department department);
        Task DeleteDepartment(string id);
        Task<List<Department>> GetAllDepartments();
        Task<Department> GetDepartmentById(string id);
        Task<ObservableCollection<Department>> GetAllDepartmentsAutomatic();

    }
}
